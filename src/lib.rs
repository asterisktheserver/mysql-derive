extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;


#[proc_macro_derive(MyRow)]
pub fn derive_myrow(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = syn::parse_derive_input(&input.to_string()).unwrap();
    let field_names: Vec<syn::Ident> = match input.body {
        syn::Body::Struct(syn::VariantData::Struct(data)) => {
            let mut idents: Vec<syn::Ident> = Vec::new();
            for field in data.into_iter() {
                idents.push(field.ident.expect("Failed to get ident from struct"));
            }
            idents
        }
        _ => panic!("#[derive(MyRow)] is to be used with structs only."),
    };
    let field_names2 = field_names.clone()
        .into_iter()
        .map(|ident| ident.as_ref().into())
        .collect::<Vec<String>>();
    let struct_name = &input.ident;
    let expanded = quote! {
        impl ::mysql::prelude::FromRow for #struct_name {
            fn from_row(row: mysql::Row) -> #struct_name {
                #struct_name::from_row_opt(row).unwrap()
            }

            fn from_row_opt(mut row: ::mysql::Row) -> Result<#struct_name, ::mysql::Error> {
                let convert_row: #struct_name = #struct_name {
                    #(
                        #field_names : row.take(#field_names2).unwrap()
                    ),*
                };
                Ok(convert_row)
            }
        }
    };
    expanded.parse().unwrap()
}
