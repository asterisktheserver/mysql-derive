# Mysql Derive

[Rust][rust] macro to automatically implement the from row trait for arbitrary structs. A simple `#[derive(MyRow)]` will generate a the nessicary code for your struct.

**This requires Rust 1.15, due to the usage of Macros 1.1.**

## How it Works

```rust
#[macro_use]
extern crate mysql_derive;
extern crate mysql;
#[derive(MyRow)]
pub struct SimpleRow {
    foo: String,
    fart: u64,
    fing: i64,
    fast: f64,
}
```

Will produce the following code at compile time

```rust
impl ::mysql::prelude::FromRow for SimpleRow {
       fn from_row(row: mysql::Row) -> SimpleRow {
           SimpleRow::from_row_opt(row).unwrap()
       }
       fn from_row_opt(mut row: ::mysql::Row) -> Result<SimpleRow, ::mysql::Error> {
           let convert_row: SimpleRow = SimpleRow {
               foo: row.take("foo").unwrap(),
               fart: row.take("fart").unwrap(),
               fing: row.take("fing").unwrap(),
               fast: row.take("fast").unwrap(),
           };
           Ok(convert_row)
       }
   }
}
```
