#[macro_use]
extern crate mysql_derive;
extern crate mysql;

#[test]
fn expands_correctly() {

    #[derive(MyRow)]
    pub struct SimpleRow {
        never: String,
        give: i64,
        you: bool,
        up: u64,
    }

}
